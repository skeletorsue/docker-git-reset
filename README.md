# Supported tags and respective ```Dockerfile``` links

* [```latest``` (*Dockerfile*)][1]

# What is This?
This is just something extremely simple I made to reset local code to match what's on the repo. 

# How do use this image
```docker run -d -v /ProjectRepoDir:/repo skeletorsue/git-reset```

[1]: https://bitbucket.org/skeletorsue/docker-git-reset/src/master/Dockerfile?fileviewer=file-view-default
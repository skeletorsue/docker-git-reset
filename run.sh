#!/bin/bash

function bail()
{
        echo 'There was an error!'
        exit 1;
}

cd /repo || bail

git fetch || bail
CNT=$(git status | grep "Your branch is behind" | wc -l)

if [ $CNT -gt 0 ];then
        git reset --hard origin/master
fi
